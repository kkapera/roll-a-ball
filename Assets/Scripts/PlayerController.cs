
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	
	public float movementSpeed;
	public GUIText countText;
	public GUIText winText;

	private int count;

	void Start() {
		count = 0;
		UpdateCountText ();
	}

	void FixedUpdate() {
		Vector3 movement;
		if (Application.platform == RuntimePlatform.Android) {
			movement = new Vector3(-Input.acceleration.y, Input.acceleration.z, 0.0F);
		} else {
			float vertical = Input.GetAxis ("Vertical");
			float horizontal = Input.GetAxis ("Horizontal");
		
			movement = new Vector3 (horizontal, 0.0F, vertical);
		}
		rigidbody.AddForce (movement * movementSpeed * Time.deltaTime);
	}
	
	void OnTriggerEnter(Collider other) {
		other.gameObject.SetActive(false);
		count++;
		UpdateCountText ();

		if (count >= 12) {
			winText.text = "YOU WON!";
		}
	}

	void UpdateCountText() {
		countText.text = "Objects collected: " + count.ToString ();
	}
}