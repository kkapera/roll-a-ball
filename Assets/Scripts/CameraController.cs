﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Transform focus;
	private Vector3 offset;
	
	void Awake() {
		offset = transform.position- focus.position;
	}
	
	void Update () {
		transform.position = focus.position + offset;
	}
}
